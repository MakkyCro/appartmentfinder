
class Person(object):
    def __init__(self, ime="", prezime="", email="", appSize=0):
        self.name = ime
        self.lastname = prezime
        self.email = email
        self.appSize = appSize

    def __repr__(self):
        return "Person(name: {}, lastname: {}, email: {}, appSize: {})".format(self.name, self.lastname, self.email, self.appSize)
