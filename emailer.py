import smtplib
import os
import jinja2
import pickle
import email
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from person import Person
from getpass import getpass


class Emailer(object):
    def __init__(self, personList=[]):
        self.personList = personList
        self.sentList = list()

        if os.path.exists('cred.txt'):
            with open('cred.txt', 'rb') as file:
                lines = file.readlines()
                if len(lines) > 0:
                    self.email = lines[0].strip().decode('utf-8')
                if len(lines) > 1:
                    self.passw = lines[1].strip().decode('utf-8')

        if os.path.exists('sentList.pickle'):
            with open('sentList.pickle', 'rb') as file:
                self.sentList = pickle.load(file)

        self.env = jinja2.Environment(loader=jinja2.FileSystemLoader(
            ""), trim_blocks=True, lstrip_blocks=True)

        if not hasattr(self, 'email'):
            self.email = input("Upiši email s kojega šaljemo:")

        if not hasattr(self, 'passw'):
            self.passw = getpass("Upiši password:")

        self.client = smtplib.SMTP('smtp.gmail.com', 587)
        self.client.starttls()
        self.client.login(self.email, self.passw)

    def saveSentList(self):
        with open('sentList.pickle', 'wb') as file:
            pickle.dump(self.sentList, file)

    def setPersonList(self, personList):
        self.personList = personList

    def sendMails(self):
        counter = 0
        for person in self.personList:
            if person.email not in self.sentList:
                model = self.getModel(person)

                emailBody = self.env.get_template(
                    "templates/email.jinja"
                ).render(model)

                msg = MIMEMultipart("alternative")
                msg["From"] = self.email
                msg["To"] = person.email
                msg["Subject"] = u'Punat smještaj'
                part1 = MIMEText(emailBody,
                                 "plain", "utf-8")
                msg.attach(part1)
                try:
                    self.client.sendmail(
                        self.email, person.email, msg.as_string().encode('ascii'))
                except BaseException:
                    print("Neuspjelo slanje za: {}.".format(person.email))
                    continue
                print("Uspjelo slanje za: {}.".format(person.email))
                counter += 1
                self.sentList.append(person.email)
                self.saveSentList()
            else:
                print("Već poslano na: {}.".format(person.email))
            print("Poslano {} emailova.".format(counter))

    def getModel(self, person):
        return {'ime_prezime': person.name, 'senderMail': self.email, 'email': person.email}

    def close(self):
        self.client.close()
