from person import Person
from emailer import Emailer
from bs4 import BeautifulSoup
from lxml import html
import requests
import re

url = 'http://www.tzpunat.hr/hr/turisticka-ponuda/privatni-smjestaj/'
page = requests.get(url)
# print(page)
soup = BeautifulSoup(page.content, 'html.parser')

lista = soup.findAll("div", {"class": "object-info"})
tree = html.fromstring(page.text)
# lista1 = tree.xpath('//ul[contains(class, "objects"]/li')

# for li in lista1:
#     print(li)

personList = []
for li in lista:
    p = li.find_all("p")[-1].text
    reg = re.compile(r'[12]/4')
    if(reg.search(p)):
        ime_prezime = li.find("h3").text
        ime_prezime = re.sub("[-–()].*$", "", ime_prezime)
        if "," in ime_prezime:
            ime = ime_prezime.split(",")[1]
            prezime = ime_prezime.split(",")[0]
        else:
            ime = ime_prezime
            prezime = ""

        if len(li.find_all("a")) > 0:
            email = li.find_all("a")[0].text
        else:
            continue

        personList.append(Person(ime, prezime, email))

emailer = Emailer()

#emailer.setPersonList([Person('Jasmin', 'Makaj', 'jasmin.makaj@gmail.com')])
emailer.setPersonList(personList)

#emailer.sendMails()
